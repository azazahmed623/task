<x-backend.master>
    <!-- Begin Page Content -->
    <div class="container-fluid">

        <!-- Page Heading -->
        <h1 class="h3 mb-2 text-gray-800">Roles</h1>

        <!-- DataTales Example -->
        <div class="card shadow mb-4">
            <div class="card-header justify-content-between py-3 d-flex">
                <h6 class="m-0 font-weight-bold text-primary">Roles Update</h6>
                <a href="{{ route('roles.index') }}">
                    <button type="button" class="btn btn-sm btn-outline-primary">
                        <span data-feather="plus"></span>
                        Roles List
                    </button>
                </a>
            </div>
            <div class="card-body">
                <div class="">
                    <div id="dataTable_wrapper" class="dataTables_wrapper dt-bootstrap4">
                        <div class="row">
                            <div class="col-sm-12">
                                <form action="{{ route('roles.update', $role->id) }}" method="POST" enctype="multipart/form-data">
                                    @csrf
                                    @method('patch')
                                    <div class="mb-3">
                                        <x-forms.input type="text" :value="old('name', $role->name)" name="name" placeholder="Name" label="Name" />
                                    </div>                   
                                    <button type="submit" class="btn btn-primary">Update</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <!-- /.container-fluid -->
</x-backend.master>
