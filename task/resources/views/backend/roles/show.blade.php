<x-backend.master>
    <!-- Begin Page Content -->
    <div class="container-fluid">

        <!-- Page Heading -->
        <h1 class="h3 mb-2 text-gray-800">Roles</h1>

        <!-- DataTales Example -->
        <div class="card shadow mb-4">
            <div class="card-header justify-content-between py-3 d-flex">
                <h6 class="m-0 font-weight-bold text-primary">Roles Show</h6>
                <a href="{{ route('roles.index') }}">
                    <button type="button" class="btn btn-sm btn-outline-primary">
                        <span data-feather="plus"></span>
                        Roles List
                    </button>
                </a>
            </div>
            <div class="card-body">
                <div class="">
                    <div id="dataTable_wrapper" class="dataTables_wrapper dt-bootstrap4">
                        <div class="row">
                            <div class="col-sm-6">
                                <table class="">
                                    <thead>
                                        <tr>
                                            <th>Name:</th>
                                            <td>{{ $role->name }}</td>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <!-- /.container-fluid -->
</x-backend.master>
