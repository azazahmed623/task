<x-backend.master>
    <!-- Begin Page Content -->
    <div class="container-fluid">

        <!-- Page Heading -->
        <h1 class="h3 mb-2 text-gray-800">Roles</h1>

        <!-- DataTales Example -->
        <div class="card shadow mb-4">
            <div class="card-header justify-content-between py-3 d-flex">
                <h6 class="m-0 font-weight-bold text-primary">Roles List</h6>
                <x-forms.message />
                <a href="{{ route('roles.create') }}">
                    <button type="button" class="btn btn-sm btn-outline-primary">
                        <span data-feather="plus"></span>
                        Add New
                    </button>
                </a>
            </div>
            <div class="card-body">
                <div class="">
                    <div id="dataTable_wrapper" class="dataTables_wrapper dt-bootstrap4">
                        <div class="row">
                            <div class="col-sm-12">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th scope="col">#Id</th>
                                            <th scope="col">Name</th>
                                            <th scope="col">Description</th>
                                            <th width="200px" scope="col">
                                                <center>Action</center>
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($roles as $role)
                                            <tr>
                                                <th scope="row">{{ $loop->iteration }}</th>
                                                <td>{{ $role->name }}</td>
                                                <td>This is description</td>
                                                <td width="200px">
                                                    <center>
                                                        @can('Admin')
                                                            <a class="btn btn-sm btn-info"
                                                                href="{{ route('roles.show', $role->id) }}">Show</a>
                                                            <a class="btn btn-sm btn-warning"
                                                                href="{{ route('roles.edit', $role->id) }}">Edit</a>
                                                            <form action="{{ route('roles.destroy', $role->id) }}"
                                                                method="post" style="display:inline">
                                                                @csrf
                                                                @method('delete')
                                                                <button class="btn btn-sm btn-danger"
                                                                    onclick="return confirm('Are you sure want to delete')">Delete</button>
                                                            </form>
                                                        @endcan
                                                    </center>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <!-- /.container-fluid -->
</x-backend.master>
