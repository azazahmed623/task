<x-backend.master>
    <!-- Begin Page Content -->
    <div class="container-fluid">

        <!-- Page Heading -->
        <h1 class="h3 mb-2 text-gray-800">Products</h1>

        <!-- DataTales Example -->
        <div class="card shadow mb-4">
            <div class="card-header justify-content-between py-3 d-flex">
                <h6 class="m-0 font-weight-bold text-primary">Products Show</h6>
                <a href="{{ route('products.index') }}">
                    <button type="button" class="btn btn-sm btn-outline-primary">
                        <span data-feather="plus"></span>
                        Products List
                    </button>
                </a>
            </div>
            <div class="card-body">
                <div class="">
                    <div id="dataTable_wrapper" class="dataTables_wrapper dt-bootstrap4">
                        <div class="row">
                            <div class="col-sm-6">
                                <img width="200px" height="350px"
                                    src="{{ asset('storage/products/' . $product->image) }}" />
                            </div>
                            <div class="col-sm-6">
                                <table class="">
                                    <thead>
                                        <tr>
                                            <th>Title:</th>
                                            <td>{{ $product->title }}</td>
                                        </tr>
                                        <tr>
                                            <th>Description:</th>
                                            <td>{{ $product->description }}</td>
                                        </tr>
                                        <tr>
                                            <th>Price:</th>
                                            <td>{{ $product->price }}</td>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <!-- /.container-fluid -->
</x-backend.master>
