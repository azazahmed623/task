<x-backend.master>
    <!-- Begin Page Content -->
    <div class="container-fluid">

        <!-- Page Heading -->
        <h1 class="h3 mb-2 text-gray-800">Products</h1>

        <!-- DataTales Example -->
        <div class="card shadow mb-4">
            <div class="card-header justify-content-between py-3 d-flex">
                <h6 class="m-0 font-weight-bold text-primary">Products Update</h6>
                <a href="{{ route('products.index') }}">
                    <button type="button" class="btn btn-sm btn-outline-primary">
                        <span data-feather="plus"></span>
                        Product List
                    </button>
                </a>
            </div>
            <div class="card-body">
                <div class="">
                    <div id="dataTable_wrapper" class="dataTables_wrapper dt-bootstrap4">
                        <div class="row">
                            <div class="col-sm-12">
                                <form action="{{ route('products.update', $product->id) }}" method="POST" enctype="multipart/form-data">
                                    @csrf
                                    @method('patch')
                                    <div class="mb-3">
                                        <x-forms.input type="text" :value="old('title', $product->title)" name="title" placeholder="Title" label="Title" />
                                    </div>
                                    <div class="mb-3">
                                        <x-forms.textarea name="description" :value="old('description', $product->description)" label="Description" id="description" />
                                    </div>
                                    <div class="mb-3">
                                        <x-forms.input type="text" :value="old('price', $product->price)" name="price" placeholder="Price" label="Price" />
                                    </div>
                                    <div class="mb-3">
                                        <x-forms.input type="file" :value="old('image', $product->image)" name="image" label="Image" />
                                    </div>
                                    <div class="mb-3">
                                    <img width="40px" height="55px"
                                                        src="{{ asset('storage/products/' . $product->image) }}" />
                                    </div>                    
                                    <button type="submit" class="btn btn-primary">Update</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <!-- /.container-fluid -->
</x-backend.master>
